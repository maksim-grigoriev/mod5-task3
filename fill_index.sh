#! /bin/bash
MYDIR=`dirname $0`
cp -fv $MYDIR/template/index.html $MYDIR/html/
sed -i "s/+color+/$1/g" $MYDIR/html/index.html
sed -i "s/+COLOR+/$2/g" $MYDIR/html/index.html
